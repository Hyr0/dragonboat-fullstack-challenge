import Projects from 'db/projects';
import Service from '../utils/Service';

import ObjectDoesNotExistError from '../utils/exceptions/ObjectDoesNotExistError';
import MissingFieldsError from '../utils/exceptions/MissingFieldsError';

export default class extends Service {
	getOne = (id) => {
		const project = Projects.findOne({ id });
		if (!project) throw new ObjectDoesNotExistError();

		return project;
	};

	getAll = () => {
		return Projects.findAll();
	};

	addNew = (data = {}) => {
		const validatedData = Service.validateData(data);
		if (validatedData.error.length) throw new MissingFieldsError(validatedData.error.join());
		return Projects.addNew(validatedData.data);
	};

	editOne = (id, data = {}) => {
		const validatedData = Service.validateData(data);
		const project = Projects.editOne(id, validatedData.data);
		if (!project) throw new ObjectDoesNotExistError();

		return project;
	};

	deleteOne = (id) => {
		const project = Projects.deleteOne(id);
		if (!project) throw new ObjectDoesNotExistError();

		return project;
	};
}
