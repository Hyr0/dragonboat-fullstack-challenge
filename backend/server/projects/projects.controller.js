import ProjectsService from './projects.service';

export default class {
	getOne = (id) => {
		const service = new ProjectsService();

		return service.getOne(id);
	};

	get = () => {
		const service = new ProjectsService();

		return service.getAll();
	};

	add = (data) => {
		const service = new ProjectsService();

		return service.addNew(data);
	};

	edit = (id, data) => {
		const service = new ProjectsService();

		return service.editOne(id, data);
	};

	delete = (id) => {
		const service = new ProjectsService();

		return service.deleteOne(id);
	};
}
