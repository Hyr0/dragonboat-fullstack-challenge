// this class is inherited by every service. use it as you find useful
export default class {
  constructor() {}

  static validateData=(data = {})=>{

    const requiredFields = ['title','author','startDate','endDate'];
    let missingFields = [];
    let serialized = {};

    requiredFields.forEach(field=>{
      if(data[field]){
        serialized[field]=data[field];
      }else{
        missingFields.push(field)
      }
    });
    
    return {
      data:serialized,
      error:missingFields
    }
  }
}
