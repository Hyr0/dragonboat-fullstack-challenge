import CustomError from './CustomError';

export default class ObjectDoesNotExistError extends CustomError {
    constructor(message, type) {
        super("Object Does Not Exist");
      }
 }
