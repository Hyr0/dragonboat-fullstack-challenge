import CustomError from './CustomError';

export default class MissingFieldsError extends CustomError {
    constructor(message, type) {
        super("Missing fields: " + message);
      }
}