// This model mocks a real database model for the sake com simplicity
const data = [
	{
		id: 1,
		title: 'Project 1',
		author: 'Author 1',
		startDate: '2021-05-30',
		endDate: '29/07/2021'
	},
	{
		id: 2,
		title: 'Project 2',
		author: 'Author 2',
		startDate: '2021-05-30',
		endDate: '2021-07-30'
	},
	{
		id: 3,
		title: 'Project 3',
		author: 'Author 3',
		startDate: '2021-05-30',
		endDate: '2021-07-30'
	},
	{
		id: 4,
		title: 'Project 4',
		author: 'Author 4',
		startDate: '2021-05-30',
		endDate: '2021-07-30'
	},
	{
		id: 5,
		title: 'Project 5',
		author: 'Author 5',
		startDate: '2021-05-30',
		endDate: '2021-07-30'
	}
];
export default class {
	// receives conditions like { title: 'Project 5' } and returns a list of matches
	static findAll = (conditions = {}) => {
		return data
			.filter((obj) =>
				Object.entries(conditions).reduce((curr, [ key, condition ]) => {
					if (!curr) return false;
					return obj[key] === condition;
				}, true)
			)
			.sort((a, b) => (a.id > b.id ? 1 : -1));
	};

	// receives conditions like { title: 'Project 5' } and returns the first match
	static findOne = (conditions = {}) => {
		return data.find((obj) =>
			Object.entries(conditions).reduce((curr, [ key, condition ]) => {
				if (!curr) return false;
				return obj[key] === condition;
			}, true)
		);
	};

	// You can add more methods to this mock to extend its functionality or
	// rewrite it to use any kind of database system (eg. mongo, postgres, etc.)
	// it is not part of the evaluation process

	static addNew = (fields = {}) => {
		let sortedData = data.sort((a, b) => (a.id > b.id ? -1 : 1));
		data.push({ id: sortedData[0].id + 1, ...fields });
		return 'Success';
	};

	static editOne = (id, fields = {}) => {
		let index = data.findIndex((item) => item.id === Number(id));
		if (index > -1) {
			data[index] = { ...data[index], ...fields };
			return 'Success';
		} else {
			return false;
		}
	};

	static deleteOne = (id) => {
		let index = data.findIndex((item) => item.id === Number(id));
		if (index > -1) {
			data.splice(index, 1);
			return 'Success';
		} else {
			return false;
		}
	};
}
