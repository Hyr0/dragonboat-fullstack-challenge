import React from 'react';
import styled from 'styled-components';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
const Component = ({ project, handleSubmit, handleChange }) => {
	return (
		<form onSubmit={handleSubmit}>
			<Title variant="h4">
				Project Form
				<Button onClick={handleSubmit}>Save</Button>
			</Title>
			<FormContainer>
				<Input value={project.title || ''} fullWidth id="title" label="Title" onChange={handleChange} />
				<Input value={project.author || ''} fullWidth id="author" label="Author" onChange={handleChange} />
				<Input
					value={project.startDate || ''}
					fullWidth
					id="startDate"
					label="Start Date"
					type="date"
					InputLabelProps={{
						shrink: true
					}}
					onChange={handleChange}
				/>
				<Input
					value={project.endDate || ''}
					fullWidth
					id="endDate"
					label="End Date"
					type="date"
					InputLabelProps={{
						shrink: true
					}}
					onChange={handleChange}
				/>
			</FormContainer>
		</form>
	);
};

const Title = styled(Typography)`
  padding: 20px 0 20px 13px;
  display:flex;
  justify-content:space-between;
`;
const Input = styled(TextField)`
  margin:10px 0px !important;
  display:block !important;
`;
const FormContainer = styled(Paper)`
  padding: 20px;
`;

export default Component;
