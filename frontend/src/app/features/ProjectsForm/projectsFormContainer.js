import React, { useEffect, useState } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import {
	fetchProject as fetchProjectAction,
	editProject as editProjectAction,
	addProject as addProjectAction
} from '../../store/projects/actions';
import { selectProject } from '../../store/projects/selectors';
import { useParams, useHistory } from 'react-router-dom';
const container = (Component) => {
	return (props) => {
		const [ projectData, setProjectData ] = useState({});

		const dispatch = useDispatch();
		const params = useParams();
		const history = useHistory();

		const newProject = params.id.toLowerCase() === 'new';

		const fetchProject = () => dispatch(fetchProjectAction(Number(params.id)));
		const project = useSelector((state) => selectProject(state, Number(params.id)));

		const handleChange = (e) => {
			setProjectData((prevState) => ({
				...prevState,
				[e.target.id]: e.target.value
			}));
		};
		const submitCallback = () => {
			history.goBack();
		};
		const handleSubmit = (e) => {
			try {
				e.preventDefault();
			} catch (error) {}
			dispatch(
				newProject
					? addProjectAction(projectData, submitCallback)
					: editProjectAction(Number(params.id), projectData, submitCallback)
			);
		};
		useEffect(
			() => {
				if (!newProject) {
					fetchProject();
				}
				// eslint-disable-next-line react-hooks/exhaustive-deps
			},[ newProject ]
		);

		return (
			<Component
				{...props}
				project={{ ...project, ...projectData }}
				handleChange={handleChange}
				handleSubmit={handleSubmit}
			/>
		);
	};
};

export default container;
