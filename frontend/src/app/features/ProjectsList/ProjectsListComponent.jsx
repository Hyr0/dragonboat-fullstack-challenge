import React, { Fragment } from 'react';
import styled from 'styled-components';

import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import DeleteIcon from '@material-ui/icons/Delete';
const Component = ({ projects, onDelete, onNavigate }) => {
	return (
		<Fragment>
			<Title variant="h4">
				Projects List
				<Button onClick={() => onNavigate('new')}>Add</Button>
			</Title>
			<TableContainer component={Paper}>
				<Table size="small">
					<TableHead>
						<TableRow>
							<TableCell>Title</TableCell>
							<TableCell>Author</TableCell>
							<TableCell>Start Date</TableCell>
							<TableCell>End Date</TableCell>
							<TableCell width={50} />
						</TableRow>
					</TableHead>
					<TableBody>
						{projects.map((p) => (
							<TableRow key={p.id} onClick={() => onNavigate(p.id)}>
								<TableCell>{p.title}</TableCell>
								<TableCell>{p.author}</TableCell>
								<TableCell>{p.startDate}</TableCell>
								<TableCell>{p.endDate}</TableCell>
								<TableCell>
									<DeleteIcon onClick={(e) => onDelete(e, p.id)} />
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
		</Fragment>
	);
};

const Title = styled(Typography)`
  padding: 20px 0 20px 13px;
  display:flex;
  justify-content:space-between;
`;

export default Component;
