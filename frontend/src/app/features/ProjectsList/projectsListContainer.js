import React, { useEffect } from 'react';

import { useSelector, useDispatch } from 'react-redux';

import {
	fetchProjects as fetchProjectsAction,
	deleteProject as deleteProjectAction
} from '../../store/projects/actions';

import { selectProjects } from '../../store/projects/selectors';

import { useHistory } from 'react-router-dom';

const container = (Component) => {
	return (props) => {
		const history = useHistory();
		const dispatch = useDispatch();

		const fetchProjects = () => dispatch(fetchProjectsAction());
		const projects = useSelector((state) => selectProjects(state));

		const deleteProject = (e, id) => {
			e.stopPropagation();
			dispatch(deleteProjectAction(id));
		};

		const handleNavigate = (id) => {
			history.push(`/${id}`);
		};

		useEffect(() => {
			fetchProjects();
			// eslint-disable-next-line react-hooks/exhaustive-deps
		}, []);

		return <Component {...props} projects={projects} onDelete={deleteProject} onNavigate={handleNavigate} />;
	};
};

export default container;
