import axios from 'axios';

import { FETCH_PROJECTS, FETCH_PROJECT } from './types';

export const fetchProjects = () => {
	return async (dispatch) =>
		dispatch({
			type: FETCH_PROJECTS,
			payload: await axios.get('/projects').then((data) => data.data)
		});
};

export const fetchProject = (id) => {
	return async (dispatch) =>
		dispatch({
			type: FETCH_PROJECT,
			payload: await axios.get(`/projects/${id}`).then((data) => data.data)
		});
};

export const deleteProject = (id) => {
	return async (dispatch) => await axios.delete(`/projects/${id}`).then((data) => dispatch(fetchProjects()));
};

export const editProject = (id, data, callback) => {
	return async (dispatch) => await axios.put(`/projects/${id}`, data).then((data) => callback());
};

export const addProject = (data, callback) => {
	return async (dispatch) => await axios.post('/projects', data).then((data) => callback());
};
