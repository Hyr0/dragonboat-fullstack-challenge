import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import { withTheme } from '@material-ui/core/styles';

import ProjectsList from './app/features/ProjectsList';
import ProjectsForm from './app/features/ProjectsForm';

const App = (props) => {
	return (
		<BrowserRouter>
			<Wrapper>
				<Content>
					<Switch>
						<Route path={'/:id'}>
							<ProjectsForm />
						</Route>
						<Route>
							<ProjectsList />
						</Route>
					</Switch>
				</Content>
			</Wrapper>
		</BrowserRouter>
	);
};

const Wrapper = styled.div`
	&&&& {
		width: 100vw;
		height: 100vh;
		display: flex;
		align-items: center;
		justify-content: center;
		margin: 0;
		padding: 0;
	}
`;

const Content = withTheme(styled.div`
	min-width: 1024px;
	max-width: 100%;
	padding: 20px;
	background-color: ${({ theme }) => theme.palette.background.grey};
`);

export default withTheme(App);
